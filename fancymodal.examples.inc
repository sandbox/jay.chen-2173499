<?php

function fancymodal_examples() {
  return '<a class="ctools-use-modal ctools-modal-modal-popup-large" href="' . url('fancymodal/nojs/form/contact_site_form') . '">Contact us</a>' . "<br>" .
    '<a class="ctools-use-modal ctools-modal-modal-popup-large" href="' . url('fancymodal/nojs/form/user_login') . '">Login</a>' . "<br>" .
    '<a href="' . url('contact') . '">' . l(t('Contact'), 'contact', array('attributes' => array('class' => array()))) . '</a>'
    ;
}
