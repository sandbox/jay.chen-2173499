<?php

function fancymodal_callback($js = NULL, $fancymodal) {
  switch ($fancymodal['type']) {
    case 'form':
      return fancymodal_form_callback($js, $fancymodal['options']['form_id']);
      break;
  }
}

function fancymodal_form_callback($js = NULL, $form_id = '') {
  // Fall back if $js is not set.
  if (!$js) {
    return drupal_get_form($form_id);
  }
  module_load_include('inc', 'contact', 'contact.pages');

  drupal_set_title(t('Log in'));

  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
    'ajax' => TRUE,
  );
  $output = ctools_modal_form_wrapper($form_id, $form_state);
  if (!empty($form_state['executed'])) {
    // We'll just overwrite the form output if it was successful.
    $output = array();
    ctools_add_js('ajax-responder');
    if (isset($_GET['destination'])) {
      $output[] = ctools_ajax_command_redirect($_GET['destination']);
    }
    else {
      $output[] = ctools_ajax_command_reload();
    }
  }
  print ajax_render($output);
}
