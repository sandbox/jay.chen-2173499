/**
 * Provide the HTML to create the modal dialog.
 */

Drupal.theme.prototype.FancyModalFluidPopup = function () {
  var html = '';

  html += '<div id="ctools-modal" class="popups-box fancymodal-fluid">';
  html += '  <div class="ctools-modal-content">';
  html += '    <div class="popups-container">';
  html += '      <div class="modal-header popups-title">';
  html += '        <span id="modal-title" class="modal-title"></span>';
  html += '        <span class="popups-close close">' + Drupal.CTools.Modal.currentSettings.closeText + '</span>';
  html += '        <div class="clear-block"></div>';
  html += '      </div>';
  html += '      <div class="modal-scroll"><div id="modal-content" class="modal-content popups-body"></div></div>';
  html += '    </div>';
  html += '  </div>';
  html += '</div>';

  return html;
}

Drupal.theme.prototype.FancyModalFluidThrobber = function () {
  var html = '';
  html += '  <div id="modal-throbber">';
  html += '    <div class="modal-throbber-wrapper">';
  html +=        'hh' + Drupal.CTools.Modal.currentSettings.throbber + 'ii';
  html += '    </div>';
  html += '  </div>';

  return html;
};
